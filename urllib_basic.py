import urllib.request
import urllib.parse

# Установил pip install BeautifulSoup4(в довесок можно pip install lxml).
from bs4 import BeautifulSoup

#Упростил поиск страницы, с которой будем парсить(напрямую ссылку указал).
url = 'https://www.w3schools.com/xml/cd_catalog.xml'

headers = {}
headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686)'

req = urllib.request.Request(url, headers=headers)
resp = urllib.request.urlopen(req)
resp_data = resp.read()

'''
вторым аргументом нужно задать ‘html.parser’, если парсишь html, очевидно. Или задать ‘xml’, если парсишь xml.
Можно исп-ть вторым аргументом 'html.parser' или исп-ть вторым аргументом ‘lxml’(в этом случае нужно установить lxml: pip install lxml)
'''
html = BeautifulSoup(resp_data, "xml")

# проитерировал список с TITLE и вывел текст каждой из них
for item in html.findAll('TITLE'):
    print(item.text)

